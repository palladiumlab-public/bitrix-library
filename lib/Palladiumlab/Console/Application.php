<?php


namespace Palladiumlab\Console;


use Symfony\Component\Console\Application as ConsoleApplication;

class Application
{
    protected static bool $initialized = false;

    private static ConsoleApplication $consoleApplication;

    protected function __construct()
    {
        if (!static::$initialized) {
            static::init();

            static::$initialized = true;
        }
    }

    public static function getConsoleApplication(): ConsoleApplication
    {
        if (!isset(static::$consoleApplication)) {
            static::$consoleApplication = new ConsoleApplication('palladiumlab');
        }

        return static::$consoleApplication;
    }

    protected static function init(): void
    {
        $app = static::$consoleApplication;

        $app->addCommands([
            new Commands\Deploy\ConstantsCommand(),
        ]);
    }
}