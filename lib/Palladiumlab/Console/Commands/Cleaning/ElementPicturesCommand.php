<?php

namespace Palladiumlab\Console\Commands\Cleaning;


use Palladiumlab\Support\Bitrix\Bitrix;
use Palladiumlab\Traits\LoggerTrait;
use CIBlock;
use CIBlockElement;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ElementPicturesCommand extends Command
{
    use LoggerTrait;

    protected static $defaultName = 'cleaning:element-pictures';

    protected function configure(): void
    {
        $this->setDescription('Удаление картинок для всех элементов');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        Bitrix::modules(['iblock']);

        $this->setLogger($this->makeLogger([
//            $this->getRotatingFileHandler(LOG_PATH . '/cleaning/element-pictures.log'),
            $this->getStreamHandler(),
        ]));

        $elementsResource = CIBlockElement::getList(
            ['ID' => 'DESC'],
            ['IBLOCK_ID' => CATALOG_ID],
            false,
            false,
            ['ID']
        );

        $progressBar = new ProgressBar($output, $elementsResource->selectedRowsCount(), 0);

        $this->info('Script started');

        $progressBar->start();

        foreach (resource_generator($elementsResource) as $elementItem) {
            $elementId = (int)$elementItem['ID'];

            $bitrixElement = new CIBlockElement();

            $bitrixElement->update($elementId, [
                'PREVIEW_PICTURE' => ['del' => 'Y'],
                'DETAIL_PICTURE' => ['del' => 'Y'],
            ]);

            CIBlockElement::SetPropertyValuesEx($elementId, CATALOG_ID, [
                'SECOND_PICTURE' => array("VALUE" => "", "DESCRIPTION" => ""),
                'MORE_PHOTO' => array(
                    0 => array("VALUE" => "", "DESCRIPTION" => "")
                ),
            ]);

            $this->info('Element pictures deleted', ['id' => $elementId]);

            /** @noinspection DisconnectedForeachInstructionInspection */
            $progressBar->advance();
        }

        CIBlock::clearIblockTagCache(CATALOG_ID);

        $progressBar->finish();

        $this->info('Script finished');

        $output->writeln('');

        return 0;
    }
}