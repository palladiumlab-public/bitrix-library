<?php

namespace Palladiumlab\Console\Commands\Cleaning;


use Palladiumlab\Support\Bitrix\Bitrix;
use Palladiumlab\Traits\FileUsageTrait;
use Palladiumlab\Traits\LoggerTrait;
use Exception;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class RemoveIntegrationFilesCommand extends Command
{
    use LoggerTrait, FileUsageTrait;

    protected const FILES_PATH = ROOT_PATH . '/upload/';

    protected static $defaultName = 'cleaning:remove-integration-files';

    protected function configure(): void
    {
        $this->setDescription('Очистка файлов импорта 1C');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        Bitrix::modules('iblock');

        $this->setLogger($this->makeLogger([
//            $this->getRotatingFileHandler(LOG_PATH . '/leaning/remove-integration-files.log'),
            $this->getStreamHandler(),
        ]));

        $this->info('Script started');

        $this->process();

        $this->info('Script finished');

        return parent::SUCCESS;
    }

    /**
     * @return void
     * @throws Exception
     */
    protected function process(): void
    {
        $finder = new Finder();

        $finder
            ->depth(0)
            ->directories()
            ->sortByName(true)
            ->reverseSorting()
            ->in(self::FILES_PATH)
            ->name("1c_catalog*");

        /** @var SplFileInfo $file */
        foreach ($finder as $directory) {
            $this->deleteDir($directory->getPathname());

            $this->info("Directory removed {$directory->getFilename()}");
        }
    }

    public function deleteDir(string $dirPath)
    {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }

        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }

        $files = array_merge(
            glob($dirPath . '*', GLOB_MARK),
            glob($dirPath . '.[^.]*', GLOB_MARK),
        );

        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }

        rmdir($dirPath);
    }
}