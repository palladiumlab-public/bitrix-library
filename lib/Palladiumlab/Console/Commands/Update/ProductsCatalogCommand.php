<?php


namespace Palladiumlab\Console\Commands\Update;


use Bitrix\Catalog\Model\Price;
use Bitrix\Currency\CurrencyManager;
use CIBlockElement;
use Palladiumlab\Support\Bitrix\Bitrix;
use Palladiumlab\Traits\LoggerTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConstantsCommand
 * @package Palladiumlab\Console\Commands\Update
 */
class ProductsCatalogCommand extends Command
{
    use LoggerTrait;

    protected static string $defaultName = 'update:products-catalog';

    protected function configure(): void
    {
        $this->setDescription('Обновление товаров под новый каталог');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        Bitrix::modules('iblock');

        $this->setLogger($this->makeLogger([
            $this->getRotatingFileHandler(LOG_PATH . '/update/element-articles.log')
        ]));

        $elementsResource = CIBlockElement::getList(
            ['ID' => 'DESC'],
            ['IBLOCK_ID' => IBLOCK_KATALOG_LEPNINI_ID],
            false,
            false,
            ['ID', 'XML_ID', 'PROPERTY_PRICE']
        );

        $progressBar = new ProgressBar($output, $elementsResource->selectedRowsCount(), 0);

        $this->info('Script started');

        $progressBar->start();

        while ($elementItem = $elementsResource->fetch()) {
            $elementId = (int)$elementItem['ID'];
            $elementPrice = (float)$elementItem['PROPERTY_PRICE_VALUE'];

            /** @noinspection PhpUnhandledExceptionInspection */
            $existedPrice = Price::getList([
                'filter' => ['PRODUCT_ID' => $elementId],
                'select' => ['ID'],
                'limit' => 1,
            ])->fetch();

            if ($existedPrice) {
                Price::update($existedPrice['ID'], [
                    'PRICE' => $elementPrice,
                ]);
            } else {
                Price::add([
                    'PRODUCT_ID' => $elementId,
                    'CATALOG_GROUP_ID' => PRICE_BASE_ID,
                    'PRICE' => $elementPrice,
                    'CURRENCY' => CurrencyManager::getBaseCurrency(),
                ]);
            }

            $this->info('Update element', ['id' => $elementId, 'price' => $elementPrice]);

            $progressBar->advance();
        }

        $progressBar->finish();

        $this->info('Script finished');

        $output->writeln('');

        return self::SUCCESS;
    }
}