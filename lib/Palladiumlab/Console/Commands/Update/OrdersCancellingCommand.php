<?php

namespace Palladiumlab\Console\Commands\Update;


use Palladiumlab\Bitrix\Sale\Order;
use Palladiumlab\Support\Bitrix\Bitrix;
use Palladiumlab\Traits\LoggerTrait;
use Bitrix\Sale\Internals\OrderTable;
use Carbon\Carbon;
use CSaleOrder;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrdersCancellingCommand extends Command
{
    use LoggerTrait;

    protected static $defaultName = 'update:orders-cancelling';

    protected function configure(): void
    {
        $this->setDescription('Отмена устаревших заказов');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        Bitrix::modules(['iblock']);

        $this->setLogger($this->makeLogger([
//            $this->getRotatingFileHandler(LOG_PATH . '/update/orders-cancelling.log'),
            $this->getStreamHandler(),
        ]));

        $ordersIdentifiers = array_map('intval', array_column(OrderTable::getList([
            'filter' => [
                '=PAYED' => 'N',
                '=CANCELED' => 'N',
            ],
            'select' => ['ID'],
        ])->fetchAll(), 'ID'));

        $progressBar = new ProgressBar($output, count($ordersIdentifiers), 0);

        $this->info('Script started');

        $progressBar->start();

        foreach ($ordersIdentifiers as $orderId) {
            /** @var Order $order */
            $order = Order::load($orderId);

            /** @noinspection NullPointerExceptionInspection */
            $orderDateCreate = Carbon::createFromTimestamp($order->getField('DATE_INSERT')->getTimestamp());
            $now = Carbon::now();

            if ($now > $orderDateCreate->addHour()) {
                CSaleOrder::CancelOrder($orderId, 'Y', 'Истёк срок ожидания оплаты');
                $order->setField('STATUS_ID', 'C');

                $this->info('Order was canceled', ['id' => $orderId]);
            }

            $order->save();

            /** @noinspection DisconnectedForeachInstructionInspection */
            $progressBar->advance();
        }

        $progressBar->finish();

        $output->writeln('');

        $this->info('Script finished');

        return 0;
    }
}