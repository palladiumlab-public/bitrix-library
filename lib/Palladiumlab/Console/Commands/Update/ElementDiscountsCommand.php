<?php

/** @noinspection PhpUndefinedClassInspection */


namespace Palladiumlab\Console\Commands\Update;


use Bitrix\Iblock\PropertyEnumerationTable;
use CCatalogDiscount;
use CIBlockElement;
use Exception;
use Palladiumlab\Traits\LoggerTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ElementCodesCommand
 * @package Palladiumlab\Console\Commands\Update
 */
class ElementDiscountsCommand extends Command
{
    use LoggerTrait;

    protected static string $defaultName = 'update:element-discounts';

    protected function configure(): void
    {
        $this->setDescription('Обновление признака "Скидка" всех элементов');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        modules(['iblock']);

        $this->setLogger($this->makeLogger([
            $this->getRotatingFileHandler(LOG_PATH . '/update/element-discounts.log'),
        ]));

        $elementsResource = CIBlockElement::getList(
            ['ID' => 'DESC'],
            ['IBLOCK_ID' => IBLOCK_KATALOG_LEPNINI_ID],
            false,
            false,
            ['ID']
        );

        $progressBar = new ProgressBar($output, $elementsResource->selectedRowsCount(), 0);

        /** @noinspection PhpMultipleClassDeclarationsInspection */
        $enumerationDiscountValueId = (int)PropertyEnumerationTable::getRow([
            'filter' => [
                '=PROPERTY.IBLOCK_ID' => IBLOCK_KATALOG_LEPNINI_ID,
                '=PROPERTY.CODE' => 'IS_SALE',
                '=VALUE' => 'Да',
            ],
            'select' => ['ID'],
        ])['ID'];

        $this->info('Script started');

        $progressBar->start();

        foreach (resource_generator($elementsResource) as $elementItem) {
            $elementId = (int)$elementItem['ID'];

            $groupAllUsersId = 2;

            $discounts = CCatalogDiscount::GetDiscount(
                $elementId, IBLOCK_KATALOG_LEPNINI_ID, [PRICE_BASE_ID], [$groupAllUsersId],
                "N", SITE_ID, false, false, false
            );

            CCatalogDiscount::ClearDiscountCache(['PRODUCT', 'SECTIONS', 'SECTION_CHAINS', 'PROPERTIES']);

            CIBlockElement::SetPropertyValuesEx($elementId, IBLOCK_KATALOG_LEPNINI_ID, [
                'IS_SALE' => !empty($discounts) ? $enumerationDiscountValueId : false,
            ]);

            $this->info('Update element', ['id' => $elementId, 'discount' => !empty($discounts)]);

            /** @noinspection DisconnectedForeachInstructionInspection */
            $progressBar->advance();

            unset($discounts, $elementId);
        }

        $progressBar->finish();

        $this->info('Script finished');

        $output->writeln('');

        return 0;
    }
}