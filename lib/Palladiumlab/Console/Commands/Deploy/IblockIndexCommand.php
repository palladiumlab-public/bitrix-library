<?php

/** @noinspection PhpMissingFieldTypeInspection */


namespace Palladiumlab\Console\Commands\Deploy;


use Bitrix\Iblock\PropertyIndex\Manager;
use CBitrixComponent;
use CIBlock;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConstantsCommand
 * @package Palladiumlab\Console\Commands\Deploy
 */
class IblockIndexCommand extends Command
{
    protected static $defaultName = 'deploy:iblock-index';

    protected function configure(): void
    {
        $this->setDescription('Генерация фасетного индекса инфоблока');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $index = Manager::createIndexer(CATALOG_ID);

        $index->startIndex();

        $index->continueIndex();

        $index->endIndex();

        Manager::checkAdminNotification();
        CBitrixComponent::clearComponentCache("bitrix:catalog.smart.filter");
        CIBlock::clearIblockTagCache(CATALOG_ID);

        $output->writeln(PHP_EOL . '    <info>Фасетный индекс собран!</info>' . PHP_EOL);

        return self::SUCCESS;
    }
}