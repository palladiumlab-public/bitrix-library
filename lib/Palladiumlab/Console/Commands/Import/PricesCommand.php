<?php

namespace Palladiumlab\Console\Commands\Import;


use Palladiumlab\Support\Bitrix\Bitrix;
use Palladiumlab\Support\Util\Arr;
use Palladiumlab\Traits\FileUsageTrait;
use Palladiumlab\Traits\LoggerTrait;
use Bitrix\Catalog\GroupTable;
use Bitrix\Catalog\Model\Price;
use Bitrix\Catalog\PriceTable;
use Bitrix\Currency\CurrencyManager;
use Bitrix\Iblock\ElementTable;
use Bitrix\Iblock\IblockTable;
use Exception;
use Illuminate\Support\Collection;
use SimpleXMLElement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\SplFileInfo;

class PricesCommand extends Command
{
    use LoggerTrait, FileUsageTrait;

    protected const FILES_PATH = ROOT_PATH . '/upload/';
    protected const IMPORT_FILES_PATH = ROOT_PATH . '/import/prices/';

    protected static $defaultName = 'import:prices';

    protected function configure(): void
    {
        $this->setDescription('Импорт цен из xml файла 1С');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        Bitrix::modules('iblock');

        $this->setLogger($this->makeLogger([
//            $this->getRotatingFileHandler(LOG_PATH . '/import/prices.log'),
            $this->getStreamHandler(),
        ]));

        $this->info('Script started');

        $this->process();

        $this->info('Script finished');

        return parent::SUCCESS;
    }

    /**
     * @return void
     * @throws Exception
     */
    protected function process(): void
    {
        /** @var SplFileInfo $pricesFile */
        $pricesFile = (new Collection(
            $this
                ->findFiles(self::FILES_PATH, 'prices___*.xml', 1)
                ->sortByModifiedTime()
                ->reverseSorting()
                ->getIterator()
        ))->first();

        if (!$pricesFile) {
            return;
        }

        $this->moveFileToProcess($pricesFile, self::IMPORT_FILES_PATH);

        $catalogInfo = IblockTable::getRowById(CATALOG_ID);

        if (!$catalogInfo) {
            $this->error('Каталог не найден');
            $this->moveFileToError($pricesFile, self::IMPORT_FILES_PATH);
            return;
        }

        $pricesXml = new SimpleXMLElement($pricesFile->getContents());

        if ((string)$pricesXml->ПакетПредложений->ИдКаталога !== $catalogInfo['XML_ID']) {
            $this->error('Файл не принадлежит каталогу');
            $this->moveFileToError($pricesFile, self::IMPORT_FILES_PATH);
            return;
        }

        $offers = $pricesXml->ПакетПредложений->Предложения->Предложение;
        /** @var SimpleXMLElement $offer */
        foreach ($offers as $offer) {
            [$elementXmlId] = explode('#', $offer->Ид);

            $element = ElementTable::getRow([
                'filter' => [
                    'IBLOCK_ID' => CATALOG_ID,
                    'XML_ID' => $elementXmlId,
                ],
                'select' => ['ID'],
            ]);

            if (!$element) {
                continue;
            }

            $elementId = $element['ID'];

            $prices = Arr::wrap($offer->Цены->Цена);

            /** @var SimpleXMLElement $price */
            foreach ($prices as $price) {
                [$priceXmlId, $priceValue] = [
                    (string)$price->ИдТипаЦены,
                    (float)$price->ЦенаЗаЕдиницу,
                ];

                $priceGroup = GroupTable::getRow(['filter' => ['XML_ID' => $priceXmlId]]);
                $priceBaseGroup = GroupTable::getRow(['filter' => ['BASE' => 'Y']]);

                foreach (array_filter([$priceGroup, $priceBaseGroup]) as $group) {
                    $existedPrice = PriceTable::getList([
                        'filter' => [
                            'PRODUCT_ID' => $elementId,
                            'CATALOG_GROUP_ID' => $group['ID'],
                        ],
                        'limit' => 1,
                    ])->fetch();

                    if ($existedPrice) {
                        $result = Price::update($existedPrice['ID'], [
                            'PRICE' => $priceValue,
                            'CATALOG_GROUP_ID' => $group['ID'],
                        ]);
                    } else {
                        $result = Price::add([
                            'PRODUCT_ID' => $elementId,
                            'PRICE' => $priceValue,
                            'CATALOG_GROUP_ID' => $group['ID'],
                            'CURRENCY' => CurrencyManager::getBaseCurrency(),
                        ]);
                    }

                    if ($result->isSuccess()) {
                        $this->info('Price saved', [
                            'productId' => $elementId,
                            'group' => $group['ID'],
                            'price' => (string)$priceValue,
                        ]);
                    } else {
                        $this->error(implode(PHP_EOL, $result->getErrorMessages()));
                    }
                }
            }
        }

        $this->moveFileToArchive($pricesFile, self::IMPORT_FILES_PATH);
    }
}