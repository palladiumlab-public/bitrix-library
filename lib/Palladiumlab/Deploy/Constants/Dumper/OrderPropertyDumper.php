<?php


namespace Palladiumlab\Deploy\Constants\Dumper;


use Palladiumlab\Support\Bitrix\Bitrix;
use Bitrix\Sale\Internals\OrderPropsTable;
use Exception;

class OrderPropertyDumper implements Dumper
{
    public function dump(): ?array
    {
        try {
            $result = false;
            if (Bitrix::modules('sale')) {
                $result = [];
                $list = OrderPropsTable::getList([
                    'select' => ['*', 'TYPE_CODE' => 'PERSON_TYPE.CODE'],
                    'order' => ['ID' => 'ASC']
                ]);
                while ($item = $list->fetch()) {
                    $result[] = [
                        'name' => $item['NAME'],
                        'code' => 'ORDER_PROPERTY_' . $item['CODE'] . '_' . $item['TYPE_CODE'] . '_ID',
                        'id' => $item['ID'],
                    ];
                }
            }
            return $result;
        } catch (Exception $e) {
            return null;
        }
    }

    public function key(): string
    {
        return 'order_property';
    }

    public function blockTitle(): string
    {
        return 'Константы свойств заказа';
    }

    public function itemTitle(array $constant): string
    {
        return "Свойство заказа \"{$constant['name']}\"";
    }
}