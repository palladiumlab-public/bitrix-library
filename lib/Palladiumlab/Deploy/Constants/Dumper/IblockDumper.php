<?php


namespace Palladiumlab\Deploy\Constants\Dumper;


use Palladiumlab\Support\Bitrix\Bitrix;
use Bitrix\Iblock\IblockSiteTable;
use Bitrix\Iblock\IblockTable;
use Exception;

class IblockDumper implements Dumper
{
    public function dump(): ?array
    {
        try {
            $result = false;
            if (Bitrix::modules('iblock')) {
                $result = [];
                $list = IblockTable::getList([
                    'order' => ['ID' => 'ASC']
                ]);
                while ($item = $list->fetch()) {
                    if (!empty($item['CODE'])) {
                        $sites = implode(', ', array_column(IblockSiteTable::getList([
                            'filter' => ['IBLOCK_ID' => $item['ID']],
                            'select' => ['SITE_ID'],
                        ])->fetchAll(), 'SITE_ID'));

                        $result[] = [
                            'name' => $item['NAME'],
                            'site_id' => $sites,
                            'code' => 'IBLOCK_' . str_replace('-', '_', strtoupper($item['CODE'])) . '_ID',
                            'id' => $item['ID'],
                        ];
                    }
                }
            }
            return $result;
        } catch (Exception $e) {
            return null;
        }
    }

    public function key(): string
    {
        return 'iblock';
    }


    public function blockTitle(): string
    {
        return 'Константы инфоблоков';
    }

    public function itemTitle(array $constant): string
    {
        return "Инфоблок {$constant['name']}, Сайты: {$constant['site_id']}";
    }
}