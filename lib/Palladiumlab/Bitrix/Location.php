<?php

namespace Palladiumlab\Bitrix;

use Palladiumlab\Support\Bitrix\CookieManager;
use Bitrix\Sale\Location\LocationTable;

class Location
{
    public const COOKIE_KEY_NAME = 'CURRENT_LOCATION';
    public const DEFAULT_CITY = 'Москва';

    protected static array $current = [];

    public static function updateCurrent(int $locationId): void
    {
        CookieManager::push(static::COOKIE_KEY_NAME, $locationId);
    }

    public static function getCurrent()
    {
        $locationId = CookieManager::get(static::COOKIE_KEY_NAME);

        if (!$locationId) {
            $locationId = static::getDefaultCityId();

            CookieManager::push(static::COOKIE_KEY_NAME, $locationId);
        }

        if (!isset(static::$current[$locationId])) {
            static::$current[$locationId] = static::getCityDataByFilter(['ID' => $locationId]);
        }

        if (empty(static::$current[$locationId])) {
            static::$current[$locationId] = static::getCityDataByFilter(['ID' => static::getDefaultCityId()]);
        }

        return static::$current[$locationId];
    }

    public static function getDefaultCityId(): int
    {
        $geoData = get_geo_data();

        if ($geoData['city']['name_ru']) {
            $cityByGeoData = (int)static::getCityDataByFilter([
                'TITLE' => $geoData['city']['name_ru'],
            ])['ID'];

            if ($cityByGeoData > 0) {
                return $cityByGeoData;
            }
        }

        return (int)static::getCityDataByFilter([
            'TITLE' => static::DEFAULT_CITY,
        ])['ID'];
    }

    public static function getCityDataByFilter(array $filter): array
    {
        return (array)LocationTable::getList([
            'select' => ['*', 'TITLE' => 'NAME.NAME'],
            'filter' => array_merge($filter, [
                'TYPE.CODE' => 'CITY',
                'NAME.LANGUAGE_ID' => LANGUAGE_ID,
            ]),
            'limit' => 1,
            'cache' => [
                'ttl' => 60 * 60 * 24 * 30 * 12,
                'cache_joins' => true,
            ]
        ])->fetch();
    }
}