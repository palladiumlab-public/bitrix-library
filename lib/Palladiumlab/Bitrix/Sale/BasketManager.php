<?php


namespace Palladiumlab\Bitrix\Sale;


use Bitrix\Catalog\Product;
use Bitrix\Catalog\ProductTable;
use Bitrix\Currency\CurrencyManager;
use Bitrix\Main\Context;
use Bitrix\Main\Error;
use Bitrix\Main\Result;
use Bitrix\Sale;
use CSaleUser;
use Palladiumlab\Support\Bitrix\Bitrix;
use Palladiumlab\Support\Util\Arr;
use Palladiumlab\Support\Util\Num;

/**
 * Class Basket
 * @package Palladiumlab\Support\Sale
 */
class BasketManager
{
    protected Sale\BasketBase $basket;

    public function __construct(Sale\BasketBase $basket)
    {
        $this->basket = $basket;
    }

    public static function createFromCurrent(): self
    {
        Bitrix::modules('sale');

        return new self(self::load());
    }

    protected static function load(): Sale\BasketBase
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Context::getCurrent()->getSite());

        self::loadDiscounts($basket);

        return $basket;
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpInternalEntityUsedInspection
     */
    public static function loadDiscounts(Sale\BasketBase $basket): void
    {
        global $USER;
        $userId = $USER->GetID() ?: CSaleUser::GetAnonymousUserID();

        $order = Order::create(SITE_ID, $userId);

        $order->appendBasket($basket);

        $discounts = $order->getDiscount();
        $showPrices = $discounts->getShowPrices();

        if (!empty($showPrices['BASKET'])) {
            foreach ($showPrices['BASKET'] as $basketCode => $data) {
                $basketItem = $basket->getItemByBasketCode($basketCode);
                if ($basketItem instanceof Sale\BasketItemBase) {
                    $basketItem->setFieldNoDemand('BASE_PRICE', $data['SHOW_BASE_PRICE']);
                    $basketItem->setFieldNoDemand('PRICE', $data['SHOW_PRICE']);
                    $basketItem->setFieldNoDemand('DISCOUNT_PRICE', $data['SHOW_DISCOUNT']);
                }
            }
        }
    }

    public function current(): Sale\BasketBase
    {
        return $this->basket;
    }

    public function setProductQuantity(int $productId, float $quantity)
    {
        if ($basketProduct = $this->findProduct($productId)) {

            /** @noinspection PhpUnhandledExceptionInspection */
            $result = $basketProduct->setField('QUANTITY', $quantity);

            /** @noinspection PhpUnhandledExceptionInspection */
            return !$result->isSuccess() ? $result : $this->basket->save();
        }

        return (new Result())->addError(new Error('Product not found'));
    }

    public function findProduct(int $productId): ?Sale\BasketItem
    {
        /** @var Sale\BasketItem $item */
        foreach ($this->basket as $item) {
            /** @noinspection PhpUnhandledExceptionInspection */
            if ((int)$item->getProductId() === $productId) {
                return $item;
            }
        }

        return null;
    }

    public function addProduct(int $productId, float $quantity, array $props = [], float $price = 0): Result
    {
        $fields = [
            'PRODUCT_ID' => $productId,
            'QUANTITY' => $quantity,
            'PROPS' => $props,
        ];

        if ($price > 0) {
            $fields = array_merge($fields, [
                'CURRENCY' => CurrencyManager::getBaseCurrency(),
                'LID' => Context::getCurrent()->getSite(),
                'PRICE' => $price,
                'CUSTOM_PRICE' => 'Y',
            ]);
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = Product\Basket::addProduct($fields);

        $this->reload();

        return $result;
    }

    public function reload(): BasketManager
    {
        $this->basket = self::load();

        return $this;
    }

    public function clear(): Result
    {
        $result = new Result();

        /** @var Sale\BasketItem $item */
        foreach ($this->basket as $item) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $tmpResult = $item->delete();

            if (!$tmpResult->isSuccess()) {
                $result = $tmpResult;
                break;
            }
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        return !$result->isSuccess() ? $result : $this->basket->save();
    }

    public function getSummary(): float
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->basket->getPrice();
    }

    public function getQuantity(): int
    {
        return array_reduce($this->basket->getBasketItems(), static function ($quantity, Sale\BasketItemBase $item) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $quantity += $item->getQuantity();

            return $quantity;
        }, 0);
    }

    public function getJsProducts(): array
    {

        return array_map(static function (BasketItem $basketItem) {
            /** @noinspection PhpUnhandledExceptionInspection */
            return [
                'id' => $basketItem->getProductId(),

                'quantity' => (int)$basketItem->getQuantity(),

                'price' => Num::parseFloat(Num::formatPrecision($basketItem->getPrice())),
            ];
        }, $this->basket->getBasketItems());
    }

    public function clearUnavailable(): BasketManager
    {
        $products = array_map(static function (BasketItem $basketItem) {
            /** @noinspection PhpUnhandledExceptionInspection */
            return $basketItem->getProductId();
        }, $this->basket->getBasketItems());

        /** @noinspection PhpUnhandledExceptionInspection */
        /** @noinspection PhpMultipleClassDeclarationsInspection */
        $productsInfo = Arr::combineKeys(ProductTable::getList([
            'filter' => ['=ID' => $products],
            'select' => ['ID', 'AVAILABLE']
        ])->fetchAll(), 'ID');

        /** @var BasketItem $basketItem */
        foreach ($this->basket->getBasketItems() as $basketItem) {
            /** @noinspection PhpUnhandledExceptionInspection */
            if ($productsInfo[$basketItem->getProductId()]['AVAILABLE'] === 'N') {
                /** @noinspection PhpUnhandledExceptionInspection */
                $this->removeProduct($basketItem->getProductId());
            }
        }

        return $this;
    }

    public function removeProduct(int $productId): Result
    {
        $result = new Result();

        if ($item = $this->findProduct($productId)) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $result = $item->delete();
        } else {
            $result->addError(new Error('Item not found'));
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = !$result->isSuccess() ? $result : $this->basket->save();

        $this->reload();

        return $result;
    }
}