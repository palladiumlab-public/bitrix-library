<?php


namespace Palladiumlab\Bitrix\Basket;


use Palladiumlab\Bitrix\Sale\BasketItem;
use Palladiumlab\Bitrix\Sale\Order;
use Palladiumlab\Support\Bitrix\Bitrix;
use Palladiumlab\Support\Util\Num;
use Palladiumlab\Traits\LazyMutationsTrait;
use Bitrix\Catalog\Product;
use Bitrix\Currency\CurrencyManager;
use Bitrix\Main\Context;
use Bitrix\Main\Error;
use Bitrix\Main\Result;
use Bitrix\Sale;
use Bitrix\Sale\BasketItemBase;
use CSaleUser;
use Exception;


/**
 * @method float price()
 * @see BasketManager::getPrice()
 *
 * @method float basePrice()
 * @see BasketManager::getBasePrice()
 *
 * @method int quantity()
 * @see BasketManager::getQuantity()
 *
 * @method array jsProducts()
 * @see BasketManager::getJsProducts()
 *
 * @method array empty()
 * @see BasketManager::isEmpty()
 *
 * Class BasketManager
 * @package Palladiumlab\Support\Sale
 */
class BasketManager
{
    use LazyMutationsTrait;

    protected Sale\Basket $basket;

    public function __construct(Sale\Basket $basket)
    {
        $this->basket = $basket;
    }

    public static function createFromCurrent(): self
    {
        Bitrix::modules('sale');

        /** @noinspection PhpUnhandledExceptionInspection */
        return new self(self::load());
    }

    /**
     * @return BasketManager
     * @throws Exception
     * @noinspection PhpInternalEntityUsedInspection
     */
    public static function createForUnavailable(): self
    {
        $currentBasket = self::load();

        /** @var Sale\Basket $basket */
        $basket = Sale\Basket::create($currentBasket->getSiteId());

        $basket->setFUserId($basket->getFUserId(true));

        /** @var BasketItemBase $item */
        foreach ($currentBasket->getBasketItems() as $item) {
            if (!$item->canBuy() || $item->isDelay()) {
                $item->setCollection($basket);
                $basket->addItem($item);
            }
        }

        return new self($basket);
    }

    /**
     * @return Sale\Basket
     * @throws Exception
     */
    protected static function load(): Sale\Basket
    {
        /** @var Sale\Basket $basket */
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Context::getCurrent()->getSite());

        self::loadDiscounts($basket);

        return $basket;
    }

    /**
     * @param Sale\BasketBase $basket
     * @noinspection PhpDocMissingThrowsInspection
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpInternalEntityUsedInspection
     */
    public static function loadDiscounts(Sale\BasketBase $basket): void
    {
        global $USER;
        $userId = $USER->GetID() ?: CSaleUser::GetAnonymousUserID();

        $order = Order::create(SITE_ID, $userId);

        $order->appendBasket($basket);

        $discounts = $order->getDiscount();
        $showPrices = $discounts->getShowPrices();

        if (!empty($showPrices['BASKET'])) {
            foreach ($showPrices['BASKET'] as $basketCode => $data) {
                $basketItem = $basket->getItemByBasketCode($basketCode);
                if ($basketItem instanceof Sale\BasketItemBase) {
                    $basketItem->setFieldNoDemand('BASE_PRICE', $data['SHOW_BASE_PRICE']);
                    $basketItem->setFieldNoDemand('PRICE', $data['SHOW_PRICE']);
                    $basketItem->setFieldNoDemand('DISCOUNT_PRICE', $data['SHOW_DISCOUNT']);
                }
            }
        }
    }

    public function current(): Sale\Basket
    {
        return $this->basket;
    }

    /**
     * @param int $productId
     * @param float $quantity
     * @return Result
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpDocMissingThrowsInspection
     */
    public function setProductQuantity(int $productId, float $quantity): Result
    {
        if ($basketProduct = $this->findProduct($productId)) {

            /** @var Result $result */
            $result = $basketProduct->setField('QUANTITY', $quantity);

            $result = !$result->isSuccess() ? $result : $this->basket->save();

            $result->setData(array_merge($result->getData(), [
                'BASKET' => $this->getJsProducts(),
            ]));

            return $result;
        }

        return (new Result())->addError(new Error('Product not found'));
    }

    public function findProduct(int $productId): ?Sale\BasketItem
    {
        /** @var Sale\BasketItem $item */
        foreach ($this->basket as $item) {
            /** @noinspection PhpUnhandledExceptionInspection */
            if ((int)$item->getProductId() === $productId) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @param int $productId
     * @param float $quantity
     * @param array $props
     * @param float $price
     * @return Result
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpDocMissingThrowsInspection
     */
    public function addProduct(int $productId, float $quantity, array $props = [], float $price = 0): Result
    {
        $fields = [
            'PRODUCT_ID' => $productId,
            'QUANTITY' => $quantity,
            'PROPS' => $props,
        ];

        if ($price > 0) {
            $fields = array_merge($fields, [
                'CURRENCY' => CurrencyManager::getBaseCurrency(),
                'LID' => Context::getCurrent()->getSite(),
                'PRICE' => $price,
                'CUSTOM_PRICE' => 'Y',
                'IGNORE_CALLBACK_FUNC' => 'Y',
            ]);
        }

        $result = Product\Basket::addProduct($fields);

        $this->reload();

        $result->setData(array_merge($result->getData(), [
            'BASKET' => $this->getJsProducts(),
        ]));

        return $result;
    }

    /**
     * @throws Exception
     */
    public function reload(): BasketManager
    {
        $this->basket = self::load();

        return $this;
    }

    public function clear(): Result
    {
        $result = new Result();

        /** @var Sale\BasketItem $item */
        foreach ($this->basket as $item) {
            /** @noinspection PhpUnhandledExceptionInspection */
            /** @var Result $tmpResult */
            $tmpResult = $item->delete();

            if (!$tmpResult->isSuccess()) {
                $result = $tmpResult;
                break;
            }
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return !$result->isSuccess() ? $result : $this->basket->save();
    }

    /**
     * @return float
     * @throws Exception
     */
    public function getPrice(): float
    {
        return $this->basket->getPrice();
    }

    /**
     * @return float
     * @throws Exception
     */
    public function getBasePrice(): float
    {
        return $this->basket->getBasePrice();
    }

    /**
     * @return int
     * @throws Exception
     */
    public function getQuantity(): int
    {
        return array_reduce($this->basket->getBasketItems(), static function ($quantity, Sale\BasketItemBase $item) {
            $quantity += $item->getQuantity();

            return $quantity;
        }, 0);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getJsProducts(): array
    {
        return array_map(static function (BasketItem $basketItem) {
            $price = Num::parseFloat(Num::formatPrecision($basketItem->getPrice()));
            $basePrice = Num::parseFloat(Num::formatPrecision($basketItem->getBasePrice()));
            $discount = Num::parseFloat(Num::formatPrecision($basketItem->getField('DISCOUNT_PRICE')));

            if ($basePrice > $price && $discount === 0.0) {
                $discount = $basePrice - $price;
            }

            return [
                'id' => $basketItem->getProductId(),
                'quantity' => (int)$basketItem->getQuantity(),
                'price' => $price,
                'basePrice' => $basePrice,
                'discount' => $discount,
                'weight' => Num::parseFloat(Num::formatPrecision($basketItem->getField('WEIGHT'))),
            ];
        }, $this->basket->getOrderableItems()->getBasketItems());
    }

    /**
     * @return Result
     * @throws Exception
     */
    public function clearUnavailable(): Result
    {
//        $products = array_map(static function (BasketItem $basketItem) {
//            return $basketItem->getProductId();
//        }, $this->basket->getBasketItems());

//        /** @noinspection PhpUnhandledExceptionInspection */
//        $productsInfo = Arr::combineKeys(ProductTable::getList([
//            'filter' => ['=ID' => $products],
//            'select' => ['ID', 'AVAILABLE']
//        ])->fetchAll(), 'ID');

        /** @var BasketItem $basketItem */
        foreach ($this->basket->getBasketItems() as $basketItem) {

            if ($basketItem->getField('CAN_BUY') !== 'Y') {
                $this->removeProduct($basketItem->getProductId());
            }
//            /** @noinspection PhpUnhandledExceptionInspection */
//            if ($productsInfo[$basketItem->getProductId()]['AVAILABLE'] === 'N') {
//                /** @noinspection PhpUnhandledExceptionInspection */
//                $this->removeProduct($basketItem->getProductId());
//            }
        }

        return new Result();
    }

    /**
     * @param int $productId
     * @return Result
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpDocMissingThrowsInspection
     */
    public function removeProduct(int $productId): Result
    {
        $result = new Result();

        if ($item = $this->findProduct($productId)) {
            $result = $item->delete();
        } else {
            $result->addError(new Error('Item not found'));
        }

        $result = !$result->isSuccess() ? $result : $this->basket->save();

        $this->reload();

        $result->setData(array_merge($result->getData(), [
            'BASKET' => $this->getJsProducts(),
        ]));

        return $result;
    }

    public function isEmpty()
    {
        return $this->basket->isEmpty();
    }
}