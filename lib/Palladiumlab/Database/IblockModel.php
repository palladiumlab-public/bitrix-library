<?php

/**
 * @noinspection PhpMultipleClassDeclarationsInspection
 * @noinspection UnknownInspectionInspection
 * @noinspection PhpDocMissingThrowsInspection
 * @noinspection PhpUnused
 */

namespace Palladiumlab\Database;


use Palladiumlab\Database\Resources\ModelNavigation;
use Palladiumlab\Database\Resources\ModelResource;
use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Main\Security\Random;
use CIBlockElement;
use CIBlockProperty;
use DateTime;
use Exception;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use ReflectionClass;
use RuntimeException;

/**
 * @property int $id
 * @property string $external_id
 * @property string $name
 * @property bool $is_active
 *
 * Class IblockModel
 * @package Palladiumlab\Models
 */
abstract class IblockModel implements Model, Arrayable, Jsonable
{
    /**
     * Соответствие свойств битрикс (ключи) полям спецификации (значения).
     * Заполняется один раз по XML_ID свойства
     * @var array
     */
    protected static array $propsMap = [];
    /** @var array */
    public array $bxFields = [];
    /** @var array */
    public array $bxProps = [];
    protected array $fields = [
        'id' => 0,
        'external_id' => '',
        'name' => '',
        'is_active' => false,
    ];
    protected array $attributes = [];
    /**
     * Соответствие полей битрикс (ключи) полям спецификации (значения)
     * В моделях-наследниках можно переопределять эту переменную, чтобы синхронизировать другие поля,
     * например ACTIVE => active, CODE => slug
     *
     * Все остальное необходимо размещать в свойствах.
     * У свойств должны быть заполнены XML_ID, по которым и идет привязка к полям спецификации.
     * @var array
     */
    protected array $fieldsMap = [
        'XML_ID' => 'external_id',
        'NAME' => 'name',
        'ID' => 'id',
        'ACTIVE' => 'is_active',
    ];
    /** @var boolean */
    protected bool $activeByDefault = true;

    public function __construct(array $fields = [])
    {
        $this->setAttributes($fields);

        if (isset($fields['id'])) {
            $filter = ['IBLOCK_ID' => static::getIblockId(), '=ID' => $this->id];
            $rows = CIBlockElement::GetList([], $filter, false, ['nTopCount' => 1]);
            if ($row = $rows->GetNextElement()) {
                $this->bxFields = $row->GetFields();
                $this->bxProps = $row->GetProperties();
            }
        }
    }

    /** Метод должен быть обязательно определен в моделях-наследниках. */
    public static function getIblockId(): int
    {
        throw new RuntimeException('Method' . __METHOD__ . ' is not implemented');
    }

    /**
     * @param int $id
     * @param bool $onlyActive
     * @return static|null
     */
    public static function findById(int $id, bool $onlyActive = true): ?IblockModel
    {
        return static::findOne(['=ID' => $id], $onlyActive);
    }

    /**
     * @param array $filter
     * @param bool $onlyActive
     * @return IblockModel|null
     */
    public static function findOne(array $filter = [], bool $onlyActive = true): ?IblockModel
    {
        $filter = array_merge($filter, ['IBLOCK_ID' => static::getIblockId()]);

        if ($onlyActive) {
            $filter['=ACTIVE'] = 'Y';
        }

        $rows = CIBlockElement::GetList([], $filter, false, ['nTopCount' => 1]);

        if ($row = $rows->GetNextElement()) {
            $fields = $row->GetFields();
            $fields['PROPERTIES'] = $row->GetProperties();
            return static::createFromBitrixFields($fields);
        }

        return null;
    }

    /**
     * @param array $fields
     * @return static
     * @noinspection PhpUnhandledExceptionInspection
     */
    public static function createFromBitrixFields(array $fields): IblockModel
    {
        $props = $fields['PROPERTIES'];
        unset($fields['PROPERTIES']);

        $item = new static();
        $item->bxFields = $fields;
        $item->bxProps = $props;

        $attributes = [];

        foreach ($item->fieldsMap as $bxKey => $specKey) {
            $attributes[$specKey] = $item->bxFields["~$bxKey"];

            if ($bxKey === 'ACTIVE') {
                $attributes[$specKey] = $item->bxFields[$bxKey] === 'Y';
            }
        }
        foreach (static::getPropsMap() as $propCode => $specKey) {

            [$property, $value] = [
                $item->bxProps[$propCode],
                $item->bxProps[$propCode]['VALUE'],
            ];

            if ($property['USER_TYPE'] === 'DateTime' && !empty($value)) {
                $attributes[$specKey] = (new DateTime($value))->format('d.m.Y H:i:s');
            }

            /** @noinspection PhpStatementHasEmptyBodyInspection */
            /** @noinspection MissingOrEmptyGroupStatementInspection */
            if ($property['PROPERTY_TYPE'] === 'N' && empty($value)) {
                //$value = '';
            }

            if (
                $property['PROPERTY_TYPE'] === 'E' && $property['USER_TYPE'] === 'EAutocomplete'
                && $property['LINK_IBLOCK_ID'] > 0 && ($model = self::getModelByIblockId($property['LINK_IBLOCK_ID']))
            ) {
                if (is_array($value) && $value) {
                    $attributes[$specKey] = array_map(static function ($modelId) use ($model) {
                        return $model::findById($modelId);
                    }, $value);
                } else if ($value) {
                    $attributes[$specKey] = $model::findById($value);
                }
            }

            if ($property['PROPERTY_TYPE'] === 'L' && $property['LIST_TYPE'] === 'C') {
                $attributes[$specKey] = $value === 'Y';
            }

            if (!isset($attributes[$specKey])) {
                $attributes[$specKey] = $value;
            }

        }

        $item->setAttributes($attributes);

        return $item;
    }

    protected static function getPropsMap(): array
    {
        $class = static::class;
        if (is_null(static::$propsMap[$class])) {
            $rows = CIBlockProperty::GetList(
                ['sort' => 'asc'],
                ['IBLOCK_ID' => static::getIblockId(), 'ACTIVE' => 'Y']
            );
            static::$propsMap[$class] = [];
            while ($row = $rows->Fetch()) {
                if ($row['XML_ID']) {
                    static::$propsMap[$class][$row['CODE']] = $row['XML_ID'];
                }
            }
        }
        return static::$propsMap[$class];
    }

    /**
     * @param int $iblockId
     * @return string|IblockModel|null
     */
    public static function getModelByIblockId(int $iblockId)
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $classname = IblockTable::getRow([
                'filter' => ['=ID' => $iblockId],
                'select' => ['XML_ID'],
            ])['XML_ID'] ?? false;

        if (!$classname) {
            return null;
        }

        $class = "Palladiumlab\\Database\\Models\\Iblock\\$classname";

        return class_exists($class) ? $class : null;
    }

    /**
     * Обертка над CIBlockElement::GetList
     *
     * @param array $sort
     * @param array $filter
     * @param bool|array $groupBy
     * @param bool|array $nav
     * @param array $select
     * @return ModelResource
     */
    public static function find(
        array      $sort = [],
        array      $filter = [],
        $groupBy = false,
        $nav = false,
        array      $select = []
    ): ModelResource
    {
        $filter['IBLOCK_ID'] = static::getIblockId();
        $rows = CIBlockElement::GetList($sort, $filter, $groupBy, $nav, $select);
        $items = [];

        while ($row = $rows->GetNextElement()) {
            $fields = $row->GetFields();
            $fields['PROPERTIES'] = $row->GetProperties();
            $items[] = static::createFromBitrixFields($fields);
        }

        return new ModelResource(new Collection($items), $nav ? new ModelNavigation(
            $rows->NavNum,
            $rows->NavPageSize,
            $rows->NavPageCount,
            $rows->NavPageNomer,
            $rows->NavRecordCount
        ) : null);
    }

    public function getModelName(): string
    {
        return (new ReflectionClass($this))->getShortName();
    }

    public function getField(string $key, $default = null)
    {
        return $this->{$key} ?? $default;
    }

    public function getBitrixField(string $key, $default = null)
    {
        return Arr::get($this->bxFields, $key, $default);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function save(): bool
    {
        $fields = $this->getDefaultIblockFields();

        if (empty($this->external_id)) {
            $this->external_id = (string)($this->id ?? Random::getString(10));
        }

        foreach ($this->fieldsMap as $key => $field) {
            if ($field === 'id' && ($this->{$field} ?? 0) <= 0) {
                continue;
            }

            if ($key === 'ACTIVE') {
                /** @noinspection PhpUnnecessaryBoolCastInspection */
                $fields[$key] = (bool)$this->{$field} ? 'Y' : 'N';
                continue;
            }

            $fields[$key] = htmlspecialcharsBack($this->{$field} ?? '');
        }

        if (empty($this->is_active) && !$this->isExists()) {
            $fields['ACTIVE'] = $this->activeByDefault === true ? 'Y' : 'N';
        }

        $el = new CIBlockElement();
        $created = true;

        $propertyValues = [];

        foreach (static::getPropsMap() as $key => $field) {

            if ($this->{$field} instanceof self) {

                /** @var IblockModel $model */
                $model = $this->{$field};

                $model->save();

                $propertyValues[$key] = $model->id;

            } else if (is_bool($this->{$field}) && $this->{$field}) {

                $propertyValues[$key] = $this->getEnumCheckBoxValue($key);

            } else {
                $propertyValues[$key] = htmlspecialcharsBack($this->{$field} ?? '');
            }

        }

        if ($this->isExists()) {

            $elementId = (int)$this->bxFields['ID'];
            if (!$el->Update($elementId, $fields)) {
                throw new Exception(sprintf('Не удалось обновить элемент: %s', strip_tags($el->LAST_ERROR)));
            }
            $created = false;

        } else {

            if (!$elementId = (int)$el->Add($fields)) {
                throw new Exception(sprintf('Не удалось создать элемент: %s', strip_tags($el->LAST_ERROR)));
            }
            $this->bxFields['ID'] = $elementId;

            $this->id = $elementId;
            $this->external_id = ($this->external_id ?? $this->bxFields['XML_ID']) ?: $elementId;

        }

        if (!empty($propertyValues)) {
            CIBlockElement::SetPropertyValuesEx($elementId, static::getIblockId(), $propertyValues);
        }

        if ($created) {
            $this->onCreate($elementId, $fields, $propertyValues);
        } else {
            $this->onUpdate($elementId, $fields, $propertyValues);
        }

        $this->onSave($elementId, $fields, $propertyValues);

        return true;
    }

    #[ArrayShape(['IBLOCK_ID' => "int", 'ACTIVE' => "string"])]
    protected function getDefaultIblockFields(): array
    {
        return [
            'IBLOCK_ID' => static::getIblockId(),
            'ACTIVE' => $this->activeByDefault === true ? 'Y' : 'N'
        ];
    }

    public function isExists(): bool
    {
        return (isset($this->bxFields['ID']) && (int)$this->bxFields['ID'] > 0);
    }

    protected function getEnumCheckBoxValue(string $propCode): int
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return (int)PropertyEnumerationTable::getRow([
            'filter' => [
                '=XML_ID' => 'Y',

                '=PROPERTY.CODE' => $propCode,
                '=PROPERTY.IBLOCK_ID' => static::getIblockId(),
            ],
            'select' => ['ID'],
        ])['ID'];
    }

    public function onCreate($elementId, $fields, $propertyValues): void
    {

    }

    public function onUpdate($elementId, $fields, $propertyValues): void
    {

    }

    public function onSave($elementId, $fields, $propertyValues): void
    {

    }

    /**
     * @param int $options
     * @return string
     * @throws JsonException
     */
    public function toJson($options = 0): string
    {
        return json_encode($this->toArray(), JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE);
    }

    public function toArray(): array
    {
        return array_map(static function ($item) {
            return $item instanceof Arrayable ? $item->toArray() : $item;
        }, $this->getAttributes());
    }

    public function getAttributes(): array
    {
        $attributes = array_merge($this->fields, $this->attributes);
        $result = [];
        foreach ($attributes as $attribute => $defaultValue) {
            $result[$attribute] = $this->{$attribute} ?? $defaultValue;
        }

        return $result;
    }

    public function setAttributes(array $newValues): void
    {
        $attributes = array_merge($this->fields, $this->attributes);

        foreach ($attributes as $attribute => $defaultValue) {
            if (isset($newValues[$attribute])) {
                $this->{$attribute} = $newValues[$attribute];
            } else {
                $this->{$attribute} = $this->{$attribute} ?? $defaultValue;
            }
        }
    }
}
