<?php

/** @noinspection PhpMultipleClassDeclarationsInspection */


namespace Palladiumlab\Database;


use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Error;
use Bitrix\Main\ORM\Data\AddResult;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Data\DeleteResult;
use Bitrix\Main\ORM\Data\UpdateResult;
use Bitrix\Main\ORM\Fields\BooleanField;
use Bitrix\Main\ORM\Fields\Field;
use Bitrix\Main\ORM\Fields\FloatField;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\StringField;
use Exception;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Collection;
use JsonException;
use ReflectionClass;
use RuntimeException;

/**
 * @property int $id
 *
 * Class D7Model
 * @package Palladiumlab\Database
 */
abstract class D7Model implements Model, Arrayable, Jsonable
{
    protected static string $primaryKey = 'ID';

    public function __construct(array $attributes = [])
    {
        $this->setAttributes($attributes);
    }

    public function setAttributes(array $newValues): void
    {
        /** @var Field $field */
        foreach (static::orm()::getMap() as $field) {
            if ($field instanceof ReferenceField) {
                continue;
            }

            $attribute = $field->getParameter('attribute');

            if (!array_key_exists($attribute, $newValues)) {
                continue;
            }

            $value = $newValues[$attribute];

            if ($field instanceof IntegerField) {
                $value = (int)$value;
            }
            if ($field instanceof FloatField) {
                $value = (float)$value;
            }
            if ($field instanceof StringField) {
                $value = (string)$value;
            }
            if ($field instanceof BooleanField) {
                $value = (bool)(int)$value;
            }

            $this->{$attribute} = $value;
        }
    }

    /**
     * @return DataManager|string
     */
    public static function orm()
    {
        throw new RuntimeException('Method' . __METHOD__ . ' is not implemented');
    }

    /**
     * @param int $id
     * @return ?static
     */
    public static function findById(int $id): ?D7Model
    {
        return static::findOne([static::$primaryKey => $id]);
    }

    /**
     * @param array $filter
     * @param array $order
     * @return ?static
     */
    public static function findOne(array $filter = [], array $order = ['ID' => 'DESC']): ?D7Model
    {
        $collection = static::findInternal([
            'filter' => $filter,
            'select' => array_keys(static::getAttributesMap()),
            'order' => $order,
            'limit' => 1,
        ]);

        return $collection?->first();
    }

    /**
     * @param array $parameters
     * @return Collection
     */
    public static function findInternal(array $parameters): Collection
    {
        try {
            $list = static::orm()::getList($parameters);

            if ($list->getSelectedRowsCount() > 0) {
                return collect($list->fetchAll())->map(
                    static fn($fields) => new static(static::fieldsToAttributes($fields))
                );
            }
        } catch (Exception $e) {
            return collect([]);
        }

        return collect([]);
    }

    /**
     * @param array $fields
     * @return array
     */
    public static function fieldsToAttributes(array $fields): array
    {
        if (empty($fields)) {
            return $fields;
        }

        $map = static::getAttributesMap();
        return collect($fields)->mapWithKeys(
            static fn($value, $field) => [$map[$field] => $value]
        )->all();
    }

    public static function getAttributesMap(): array
    {
        return collect(static::orm()::getMap())->mapWithKeys(
            static fn(Field $field) => [$field->getName() => $field->getParameter('attribute')]
        )->filter()->all();
    }

    /**
     * @param array $filter
     * @param array $order
     * @return Collection
     */
    public static function find(array $filter = [], array $order = []): Collection
    {
        return static::findInternal([
            'filter' => $filter,
            'select' => array_keys(static::getAttributesMap()),
            'order' => $order,
        ]);
    }

    /**
     * @param array $attributes
     * @return array
     */
    public static function attributesToFields(array $attributes): array
    {
        if (empty($attributes)) {
            return $attributes;
        }

        $map = array_flip(static::getAttributesMap());
        return collect($attributes)->mapWithKeys(
            static fn($value, $attribute) => [$map[$attribute] => $value]
        )->all();
    }

    /**
     * @return UpdateResult|AddResult
     * @throws Exception
     */
    public function save()
    {
        $fields = [];

        $map = array_flip(static::getAttributesMap());
        foreach ($map as $attribute => $field) {
            if (isset($this->{$attribute})) {
                $fields[$field] = $this->{$attribute};
            }
        }

        unset($fields['ID']);

        if ($this->isExists()) {
            return static::orm()::update($this->id, $fields);
        }

        $result = static::orm()::add($fields);

        if ($result->isSuccess()) {
            $this->id = $result->getId();
            $this->reset();
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isExists(): bool
    {
        if ($this->id <= 0) {
            return false;
        }

        try {
            return static::getCount([static::$primaryKey => $this->id]) > 0;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function getCount(array $filter = []): int
    {
        try {
            return static::orm()::getCount($filter);
        } catch (Exception $e) {
            return 0;
        }
    }

    protected function reset(): void
    {
        if ($this->id <= 0) {
            return;
        }

        try {
            $fields = static::orm()::getRow([
                'filter' => [static::$primaryKey => $this->id],
                'select' => array_keys(static::getAttributesMap())
            ]);

            if ($fields) {
                $this->setAttributes(static::fieldsToAttributes($fields));
            }
        } catch (Exception $e) {
            return;
        }
    }

    /**
     * @return DeleteResult
     */
    public function remove(): DeleteResult
    {
        $result = new DeleteResult();

        if ($this->id <= 0) {
            $result->addError(new Error('Element does not exist'));
        }

        try {
            $result = static::orm()::delete($this->id);

            if ($result->isSuccess()) {
                $this->id = 0;
            }

            return $result;
        } catch (Exception $e) {
            $result->addError(new Error($e->getMessage()));
        }

        return $result;
    }

    /**
     * @param int $options
     * @return string
     * @throws JsonException
     */
    public function toJson($options = 0): string
    {
        return json_encode($this->toArray(), JSON_THROW_ON_ERROR | $options);
    }

    public function toArray(): array
    {
        return $this->getAttributes();
    }

    public function getAttributes(): array
    {
        $fields = [];

        $map = static::getAttributesMap();
        foreach ($map as $attribute) {
            if (isset($this->{$attribute})) {
                $fields[$attribute] = $this->{$attribute};
            }
        }

        return $fields;
    }

    public function getModelName(): string
    {
        return (new ReflectionClass($this))->getShortName();
    }
}