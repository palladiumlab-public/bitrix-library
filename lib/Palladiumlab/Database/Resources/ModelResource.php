<?php


namespace Palladiumlab\Database\Resources;


use Illuminate\Support\Collection;

/**
 * @property Collection collection
 * @property ModelNavigation|null navigation
 *
 * Class ModelResource
 * @package Palladiumlab\Database\Resources
 */
class ModelResource
{
    public function __construct(Collection $collection, ?ModelNavigation $navigation)
    {
        $this->collection = $collection;
        $this->navigation = $navigation;
    }
}