<?php


namespace Palladiumlab\Database\Resources;


use Illuminate\Contracts\Support\Arrayable;

/**
 * @property-read int num
 * @property-read int pageSize
 * @property-read int pageCount
 * @property-read int pageNumber
 * @property-read int recordCount
 *
 * Class ModelNavigation
 * @package Palladiumlab\Database\Resources
 */
class ModelNavigation implements Arrayable
{
    public function __construct(int $num, int $pageSize, int $pageCount, int $pageNumber, int $recordCount)
    {
        $this->num = $num;

        $this->pageSize = $pageSize;
        $this->pageCount = $pageCount;
        $this->pageNumber = $pageNumber;

        $this->recordCount = $recordCount;
    }

    public function toArray(): array
    {
        return [
            'NUM' => $this->num,

            'PAGE_SIZE' => $this->pageSize,
            'PAGE_COUNT' => $this->pageCount,
            'PAGE_NUMBER' => $this->pageNumber,

            'RECORD_COUNT' => $this->recordCount,
        ];
    }
}
