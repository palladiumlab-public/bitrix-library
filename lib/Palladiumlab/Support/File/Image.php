<?php


namespace Palladiumlab\Support\File;


use CFile;
use Palladiumlab\Support\Bitrix\Cache;
use Throwable;

class Image
{
    protected const CACHE_TIME = 60 * 60 * 24 * 30 * 3;

    protected const VALID_EXTENSIONS = [
        'jpg',
        'jpeg',
        'gif',
        'png',
    ];

    public static function getResizePath(int $imageId, int $width, int $height, int $resizeType = BX_RESIZE_IMAGE_EXACT)
    {
        $size = ['width' => $width, 'height' => $height];

        $cacheKey = md5(__FUNCTION__) . "_$imageId" . serialize($size);

        return (new Cache($cacheKey, 'images/resize', self::CACHE_TIME))
            ->make(static function () use ($size, $imageId, $resizeType) {
                if ($imageId > 0) {
                    return CFile::ResizeImageGet($imageId, $size, $resizeType)['src'];
                }

                return CFile::GetFileArray($imageId)['SRC'];
            });
    }

    public static function getCompressedPath(string $path)
    {
        [$directory, , $extension, $fileName] = array_values(pathinfo($path));
        if (static::isValidExtension($extension)) {
            [$compressedPath, $newFileName] = [
                "$directory/{$fileName}_cmp.$extension",
                "{$fileName}_cmp",
            ];
            if (file_exists($compressedPath)) {
                return static::getLocalPath($compressedPath);
            }

            $compressor = new Compressor($path, $newFileName, $directory);
            try {
                $compressor->compressImage();

                if (file_exists($compressedPath)) {
                    return static::getLocalPath($compressedPath);
                }
            } catch (Throwable $e) {
                return static::getLocalPath($path);
            }
        }

        return static::getLocalPath($path);
    }

    protected static function isValidExtension(string $extension): bool
    {
        return in_array($extension, static::VALID_EXTENSIONS);
    }

    protected static function getLocalPath(string $path)
    {
        return str_replace($_SERVER['DOCUMENT_ROOT'], '', $path);
    }
}