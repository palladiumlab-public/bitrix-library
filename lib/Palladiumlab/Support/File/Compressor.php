<?php

namespace Palladiumlab\Support\File;


use Exception;

class Compressor
{
    protected string $filePath;
    protected string $newImageName;
    protected int $quality;
    protected int $pngQuality;
    protected string $destination;

    public function __construct(string $filePath, string $newImageName, string $destination)
    {
        $this->setFilePath($filePath);
        $this->setNewImageName($newImageName);
        $this->setQuality(60);
        $this->setPngQuality(9);
        $this->setDestination($destination);
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function setFilePath(string $path): void
    {
        $this->filePath = $path;
    }

    public function getNewImageName(): string
    {
        return $this->newImageName;
    }

    public function setNewImageName(string $name): void
    {
        $this->newImageName = $name;
    }

    public function getQuality(): int
    {
        return $this->quality;
    }

    public function setQuality($quality): void
    {
        $this->quality = $quality;
    }

    public function setPngQuality($pngQuality): void
    {
        $this->pngQuality = $pngQuality;
    }

    public function getDestination(): string
    {
        return $this->destination;
    }

    public function setDestination($destination): void
    {
        $this->destination = $destination;
    }

    /**
     * Function to compress image
     * @return string
     * @throws Exception
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public function compressImage(): string
    {

        //Send image array
        $imgTypes = ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png'];
        $newImage = null;
        $lastChar = null;
        $imageExtension = null;
        $destinationExtension = null;
        $pngCompression = null;
        $maxsize = 5245330;

        //If not found the file
        if (empty($this->filePath) && !file_exists($this->filePath)) {
            throw new Exception('Please inform the image!');
        }

        //Get image width, height, mimetype, etc..
        $image_data = getimagesize($this->filePath);
        //Set MimeType on variable
        $imageMime = $image_data['mime'];

        //Verifiy if the file is a image
        if (!in_array($imageMime, $imgTypes, true)) {
            throw new Exception('Please send a image!');
        }

        //Get file size
        $image_size = filesize($this->filePath);

        //if image size is bigger than 5mb
        if ($image_size >= $maxsize) {
            throw new Exception('Please send a imagem smaller than 5mb!');
        }

        //If not found the destination
        if (empty($this->newImageName)) {
            throw new Exception('Please inform the destination name of image!');
        }

        //If not found the quality
        if (empty($this->quality)) {
            throw new Exception('Please inform the quality!');
        }

        //If not found the png quality
        $pngCompression = (!empty($this->pngQuality)) ? $this->pngQuality : 9;

        $imageExtension = pathinfo($this->filePath, PATHINFO_EXTENSION);
        //Verify if is sended a destination file name with extension
        $destinationExtension = pathinfo($this->newImageName, PATHINFO_EXTENSION);
        //if empty
        if (empty($destinationExtension)) {
            $this->newImageName .= '.' . $imageExtension;
        }

        //Verify if folder destination isn´t empty
        if (!empty($this->destination)) {

            //And verify the last one element of value
            $lastChar = substr($this->destination, -1);

            if ($lastChar !== '/') {
                $this->destination .= '/';
            }
        }

        //Switch to find the file type
        switch ($imageMime) {
            //if is JPG and siblings
            case 'image/jpeg':
            case 'image/pjpeg':
                //Create a new jpg image
                $newImage = imagecreatefromjpeg($this->filePath);
                imagejpeg($newImage, $this->destination . $this->newImageName, $this->quality);
                break;
            //if is PNG and siblings
            case 'image/png':
            case 'image/x-png':
                //Create a new png image
                $newImage = imagecreatefrompng($this->filePath);
                imagealphablending($newImage, false);
                imagesavealpha($newImage, true);
                imagepng($newImage, $this->destination . $this->newImageName, $pngCompression);
                break;
            // if is GIF
            case 'image/gif':
                //Create a new gif image
                $newImage = imagecreatefromgif($this->filePath);
                imagealphablending($newImage, false);
                imagesavealpha($newImage, true);
                imagegif($newImage, $this->destination . $this->newImageName);
        }

        //Return the new image resized
        return $this->newImageName;
    }
}