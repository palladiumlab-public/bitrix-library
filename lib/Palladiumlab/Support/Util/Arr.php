<?php


namespace Palladiumlab\Support\Util;


use Illuminate\Support\Arr as IlluminateArr;

class Arr extends IlluminateArr
{
    public static function combineKeys(array $array, string $key): array
    {
        return array_combine(array_column($array, $key), array_values($array));
    }

    public static function shift(&$array, int $length = 1): array
    {
        $result = [];

        if (empty($array)) {
            return $result;
        }

        for ($index = 0; $index < $length; $index++) {
            $result[] = array_shift($array);
        }

        return array_filter($result);
    }

    public static function lowerKeys(array $fields): array
    {
        return array_combine(
            array_map([Str::class, 'lower'], array_keys($fields)),
            array_values($fields),
        );
    }

    public static function upperKeys(array $fields): array
    {
        return array_combine(
            array_map([Str::class, 'upper'], array_keys($fields)),
            array_values($fields),
        );
    }
}