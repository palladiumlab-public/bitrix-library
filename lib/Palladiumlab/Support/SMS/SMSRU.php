<?php

/**
 * @noinspection PhpUnused
 * @noinspection SpellCheckingInspection
 */

namespace Palladiumlab\Support\SMS;

use stdClass;


class SMSRU
{
    protected const API_KEY = '1A36141E-C136-4BB1-A50D-9197E129486A';

    protected string $apiKey;
    protected string $protocol = 'https';
    protected string $domain = 'sms.ru';
    protected int $count_repeat = 5; //количество попыток достучаться до сервера если он не доступен


    public function __construct(string $apiKey = '')
    {
        if (empty($apiKey)) {
            $apiKey = self::API_KEY;
        }

        $this->apiKey = $apiKey;
    }

    /**
     * Совершает отправку СМС сообщения одному или нескольким получателям.
     * @param $post
     *   $post->to = string - Номер телефона получателя (либо несколько номеров, через запятую — до 100 штук за один запрос). Если вы указываете несколько номеров и один из них указан неверно, то на остальные номера сообщения также не отправляются, и возвращается код ошибки.
     *   $post->msg = string - Текст сообщения в кодировке UTF-8
     *   $post->multi = array('номер получателя' => 'текст сообщения') - Если вы хотите в одном запросе отправить разные сообщения на несколько номеров, то воспользуйтесь этим параметром (до 100 сообщений за 1 запрос). В этом случае параметры to и text использовать не нужно
     *   $post->from = string - Имя отправителя (должно быть согласовано с администрацией). Если не заполнено, в качестве отправителя будет указан ваш номер.
     *   $post->time = Если вам нужна отложенная отправка, то укажите время отправки. Указывается в формате UNIX TIME (пример: 1280307978). Должно быть не больше 7 дней с момента подачи запроса. Если время меньше текущего времени, сообщение отправляется моментально.
     *   $post->translit = 1 - Переводит все русские символы в латинские. (по умолчанию 0)
     *   $post->test = 1 - Имитирует отправку сообщения для тестирования ваших программ на правильность обработки ответов сервера. При этом само сообщение не отправляется и баланс не расходуется. (по умолчанию 0)
     *   $post->partner_id = int - Если вы участвуете в партнерской программе, укажите этот параметр в запросе и получайте проценты от стоимости отправленных сообщений.
     * @return array|mixed|stdClass
     */
    public function sendOne($post)
    {
        $url = $this->protocol . '://' . $this->domain . '/sms/send';
        $request = $this->request($url, $post);
        $resp = $this->checkReplyError($request, 'send');

        if ($resp->status === "OK") {
            $temp = (array)$resp->sms;
            unset($resp->sms);

            $temp = array_pop($temp);

            if ($temp) {
                return $temp;
            }

            return $resp;
        }

        return $resp;

    }

    public function send($post)
    {
        $url = $this->protocol . '://' . $this->domain . '/sms/send';
        $request = $this->request($url, $post);
        return $this->checkReplyError($request, 'send');
    }

    /**
     * Отправка СМС сообщений по электронной почте
     * @param $post
     *   $post->from = string - Ваш электронный адрес
     *   $post->charset = string - кодировка переданных данных
     *   $post->send_charset = string - кодировка переданных письма
     *   $post->subject = string - тема письма
     *   $post->body = string - текст письма
     * @return bool
     */
    public function sendSmtp($post): bool
    {
        $post->to = $this->apiKey . '@' . $this->domain;
        $post->subject = $this->smsMimeHeaderEncode($post->subject, $post->charset, $post->send_charset);
        if ((string)$post->charset !== (string)$post->send_charset) {
            $post->body = iconv($post->charset, $post->send_charset, $post->body);
        }
        $headers = "From: $post->\r\n";
        $headers .= "Content-type: text/plain; charset=$post->send_charset\r\n";
        return mail($post->to, $post->subject, $post->body, $headers);
    }

    public function getStatus($id)
    {
        $url = $this->protocol . '://' . $this->domain . '/sms/status';

        $post = new stdClass();
        $post->sms_id = $id;

        $request = $this->request($url, $post);
        return $this->checkReplyError($request, 'getStatus');
    }

    /**
     * Возвращает стоимость сообщения на указанный номер и количество сообщений, необходимых для его отправки.
     * @param $post
     *   $post->to = string - Номер телефона получателя (либо несколько номеров, через запятую — до 100 штук за один запрос) Если вы указываете несколько номеров и один из них указан неверно, то возвращается код ошибки.
     *   $post->text = string - Текст сообщения в кодировке UTF-8. Если текст не введен, то возвращается стоимость 1 сообщения. Если текст введен, то возвращается стоимость, рассчитанная по длине сообщения.
     *   $post->translit = int - Переводит все русские символы в латинские
     * @return mixed|stdClass
     */
    public function getCost($post)
    {
        $url = $this->protocol . '://' . $this->domain . '/sms/cost';
        $request = $this->request($url, $post);
        return $this->checkReplyError($request, 'getCost');
    }

    /**
     * Получение состояния баланса
     */
    public function getBalance()
    {
        $url = $this->protocol . '://' . $this->domain . '/my/balance';
        $request = $this->request($url);
        return $this->checkReplyError($request, 'getBalance');
    }

    /**
     * Получение текущего состояния вашего дневного лимита.
     */
    public function getLimit()
    {
        $url = $this->protocol . '://' . $this->domain . '/my/limit';
        $request = $this->request($url);
        return $this->checkReplyError($request, 'getLimit');
    }

    /**
     * Получение списка отправителей
     */
    public function getSenders()
    {
        $url = $this->protocol . '://' . $this->domain . '/my/senders';
        $request = $this->request($url);
        return $this->checkReplyError($request, 'getSenders');
    }

    /**
     * Проверка номера телефона и пароля на действительность.
     * @param $post
     *   $post->login = string - номер телефона
     *   $post->password = string - пароль
     * @return mixed|stdClass
     */
    public function authCheck($post)
    {
        $url = $this->protocol . '://' . $this->domain . '/auth/check';
        $post->api_id = 'none';
        $request = $this->request($url, $post);
        return $this->checkReplyError($request, 'AuthCheck');
    }


    /**
     * На номера, добавленные в стоплист, не доставляются сообщения (и за них не списываются деньги)
     * @param string $phone Номер телефона.
     * @param string $text Примечание (доступно только вам).
     * @return mixed|stdClass
     */
    public function addStopList(string $phone, string $text = "")
    {
        $url = $this->protocol . '://' . $this->domain . '/stoplist/add';

        $post = new stdClass();
        $post->stoplist_phone = $phone;
        $post->stoplist_text = $text;

        $request = $this->request($url, $post);
        return $this->checkReplyError($request, 'addStopList');
    }

    /**
     * Удаляет один номер из стоплиста
     * @param string $phone Номер телефона.
     * @return mixed|stdClass
     */
    public function delStopList(string $phone)
    {
        $url = $this->protocol . '://' . $this->domain . '/stoplist/del';

        $post = new stdClass();
        $post->stoplist_phone = $phone;

        $request = $this->request($url, $post);
        return $this->checkReplyError($request, 'delStopList');
    }

    /**
     * Получить номера занесённые в стоплист
     */
    public function getStopList()
    {
        $url = $this->protocol . '://' . $this->domain . '/stoplist/get';
        $request = $this->request($url);
        return $this->checkReplyError($request, 'getStopList');
    }

    /**
     * Позволяет отправлять СМС сообщения, переданные через XML компании UCS, которая создала ПО R-Keeper CRM (RKeeper). Вам достаточно указать адрес ниже в качестве адреса шлюза и сообщения будут доставляться автоматически.
     */
    public function ucsSms(): stdClass
    {
        $url = $this->protocol . '://' . $this->domain . '/ucs/sms';

        $this->request($url);

        $output = new stdClass();

        $output->status = "OK";
        $output->status_code = '100';

        return $output;
    }

    /**
     * Добавить URL Callback системы на вашей стороне, на которую будут возвращаться статусы отправленных вами сообщений
     * @param $post
     *    $post->url = string - Адрес обработчика (должен начинаться на http://)
     * @return mixed|stdClass
     */
    public function addCallback($post)
    {
        $url = $this->protocol . '://' . $this->domain . '/callback/add';
        $request = $this->request($url, $post);
        return $this->checkReplyError($request, 'addCallback');
    }

    /**
     * Удалить обработчик, внесенный вами ранее
     * @param $post
     *   $post->url = string - Адрес обработчика (должен начинаться на http://)
     * @return mixed|stdClass
     */
    public function delCallback($post)
    {
        $url = $this->protocol . '://' . $this->domain . '/callback/del';
        $request = $this->request($url, $post);
        return $this->checkReplyError($request, 'delCallback');
    }

    /**
     * Все имеющиеся у вас обработчики
     */
    public function getCallback()
    {
        $url = $this->protocol . '://' . $this->domain . '/callback/get';
        $request = $this->request($url);
        return $this->checkReplyError($request, 'getCallback');
    }

    /** @noinspection CurlSslServerSpoofingInspection */
    protected function request($url, stdClass $post = null)
    {
        $r_post = $post;

        $ch = curl_init($url . "?json=1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        if (!$post) {
            $post = new stdClass();
        }

        /** @noinspection MissingOrEmptyGroupStatementInspection */
        /** @noinspection PhpStatementHasEmptyBodyInspection */
        if (!empty($post->api_id) && $post->api_id === 'none') {

        } else {
            $post->api_id = $this->apiKey;
        }

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query((array)$post));

        $body = curl_exec($ch);
        if ($body === false) {
            $error = curl_error($ch);
        } else {
            $error = false;
        }
        curl_close($ch);
        if ($error && $this->count_repeat > 0) {
            $this->count_repeat--;
            return $this->request($url, $r_post);
        }
        return $body;
    }

    /** @noinspection PhpUnusedParameterInspection */
    protected function checkReplyError($res, $action)
    {

        if (!$res) {
            $temp = new stdClass();
            $temp->status = "ERROR";
            $temp->status_code = "000";
            $temp->status_text = "Невозможно установить связь с сервером SMS.RU. Проверьте - правильно ли указаны DNS сервера в настройках вашего сервера (nslookup sms.ru), и есть ли связь с интернетом (ping sms.ru).";
            return $temp;
        }

        /** @noinspection JsonEncodingApiUsageInspection */
        $result = json_decode($res);

        if (!$result || !$result->status) {
            $temp = new stdClass();
            $temp->status = "ERROR";
            $temp->status_code = "000";
            $temp->status_text = "Невозможно установить связь с сервером SMS.RU. Проверьте - правильно ли указаны DNS сервера в настройках вашего сервера (nslookup sms.ru), и есть ли связь с интернетом (ping sms.ru)";
            return $temp;
        }

        return $result;
    }

    protected function smsMimeHeaderEncode($str, $post_charset, $send_charset): string
    {
        if ((string)$post_charset !== (string)$send_charset) {
            $str = iconv($post_charset, $send_charset, $str);
        }
        return "=?" . $send_charset . "?B?" . base64_encode($str) . "?=";
    }
}
