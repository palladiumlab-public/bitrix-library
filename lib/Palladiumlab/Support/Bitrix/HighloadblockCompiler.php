<?php


namespace Palladiumlab\Support\Bitrix;


use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\ORM\Data\DataManager;

class HighloadblockCompiler
{
    protected static array $compiled = [];

    /**
     * @param string $table
     * @return DataManager
     * @noinspection PhpMissingReturnTypeInspection
     * @noinspection ReturnTypeCanBeDeclaredInspection
     */
    public static function getByTable(string $table)
    {
        Bitrix::modules('highloadblock');

        return self::getByFilter(['=TABLE_NAME' => $table]);
    }

    /**
     * @param int $id
     * @return DataManager
     * @noinspection PhpMissingReturnTypeInspection
     * @noinspection ReturnTypeCanBeDeclaredInspection
     */
    public static function getById(int $id)
    {
        Bitrix::modules('highloadblock');

        return self::getByFilter(['=ID' => $id]);
    }

    /**
     * @param array $filter
     * @return DataManager
     * @noinspection PhpDocMissingThrowsInspection
     * @noinspection PhpMissingReturnTypeInspection
     * @noinspection ReturnTypeCanBeDeclaredInspection
     */
    public static function getByFilter(array $filter)
    {
        Bitrix::modules('highloadblock');

        $key = md5(serialize($filter));

        if (!isset(static::$compiled[$key])) {
            /** @noinspection PhpUnhandledExceptionInspection */
            /** @noinspection PhpMultipleClassDeclarationsInspection */
            /** @var DataManager $manager */
            $manager = HighloadBlockTable::compileEntity(HighloadBlockTable::getRow([
                'filter' => $filter,
                'cache' => [
                    'ttl' => 60 * 60 * 24 * 30 * 6,
                    'cache_joins' => true,
                ],
            ]));

            /** @noinspection PhpUndefinedMethodInspection */
            static::$compiled[$key] = $manager->getDataClass();
        }

        return static::$compiled[$key];
    }
}