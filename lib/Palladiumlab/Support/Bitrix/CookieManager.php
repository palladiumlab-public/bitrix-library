<?php


namespace Palladiumlab\Support\Bitrix;


use Bitrix\Main\Context;
use Bitrix\Main\Web\Cookie as WebCookie;
use Bitrix\Main\Web\CryptoCookie;

class CookieManager
{
    public static function push(string $name, string $value, ?int $time = null, bool $encrypt = true): void
    {
        Context::getCurrent()
            ->getResponse()
            ->addCookie(static::make($name, $value, $time, $encrypt))
            ->writeHeaders();
    }

    public static function make(string $name, ?string $value, ?int $time = null, bool $encrypt = true): WebCookie
    {
        $cookieClass = $encrypt ? CryptoCookie::class : WebCookie::class;

        return new $cookieClass($name, $value, $time ? time() + $time : null);
    }

    public static function forget(string $name): void
    {
        Context::getCurrent()
            ->getResponse()
            ->addCookie(static::make($name, null, time() - 3600, false))
            ->writeHeaders();
    }

    public static function get(string $name): ?string
    {
        return Context::getCurrent()
            ->getRequest()
            ->getCookie($name);
    }
}