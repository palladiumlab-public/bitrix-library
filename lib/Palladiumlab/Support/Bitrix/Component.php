<?php

namespace Palladiumlab\Support\Bitrix;

use CComponentEngine;

class Component
{
    public static function rebuildMenu(array $bitrixMenu)
    {
        $menuItems = $bitrixMenu;

        /** rebuild menu as tree */
        if (!empty($menuItems)) {
            $formattedResult = [];
            $parents = [];
            foreach ($menuItems as $index => $item) {
                if (empty($parents) || (int)$item['DEPTH_LEVEL'] === 1) {
                    $parent = &$formattedResult;
                } else {
                    $parent = &$parents[$item['DEPTH_LEVEL'] - 1];
                }
                $parent['CHILDREN'][$index] = $item;
                if ($item['IS_PARENT']) {
                    $parents[$item['DEPTH_LEVEL']] = &$parent['CHILDREN'][$index];
                }
            }
            $menuItems = $formattedResult['CHILDREN'];
        }

        return $menuItems;
    }

    public static function getSmartFilterPathFromUrl(string $sefFolder, string $filterPath = 'filter/#SMART_FILTER_PATH#/apply/')
    {
        $sefFolder = trim($sefFolder, '/');

        $defaultTemplates = ["smart_filter" => $filterPath];
        $engine = new CComponentEngine();

        if (Bitrix::modules('iblock')) {
            $engine->addGreedyPart("#SMART_FILTER_PATH#");
        }

        $urlTemplates = CComponentEngine::makeComponentUrlTemplates($defaultTemplates, []);

        $engine->guessComponentPath(
            "/$sefFolder/",
            $urlTemplates,
            $variables
        );

        return $variables['SMART_FILTER_PATH'] ?: '';
    }
}
