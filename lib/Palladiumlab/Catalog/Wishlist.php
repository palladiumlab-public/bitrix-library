<?php


namespace Palladiumlab\Catalog;

use Palladiumlab\Http\Controller\Api\ItemsAdditionalController;
use Palladiumlab\Management\User;
use Palladiumlab\Orm\WishlistTable;
use Palladiumlab\Support\Util\Arr;
use Palladiumlab\Traits\LazyMutationsTrait;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Error;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\Result;

/**
 * @method int count()
 * @see Wishlist::getCount()
 *
 * @method array state()
 * @see Wishlist::getState()
 *
 * @method static self current()
 * @see Wishlist::getCurrent()
 *
 * @method boolean empty()
 * @see Wishlist::isEmpty()
 *
 * @method boolean exists()
 * @see Wishlist::isExists()
 *
 * Working with current user only (authorized / unauthorized)
 *
 * Class Wishlist
 * @package Palladiumlab\Catalog
 */
class Wishlist
{
    use LazyMutationsTrait;

    protected array $state = [];

    protected static array $instances = [];

    protected function __construct()
    {
        if (empty($this->state)) {

            $userKey = User::getUserKey();
            $cookieKey = User::getCookieKey();

            if (!empty($cookieKey) && User::isAuthorized()) {
                $cookieState = $this->checkState(
                    $this->loadCurrentState($cookieKey)
                );

                foreach ($cookieState as $productId) {
                    $this->push($productId);
                }

                User::forgetCookieKey();
            }

            $this->state = $this->checkState(
                $this->loadCurrentState($userKey)
            );
        }
    }

    protected static function orm(): DataManager
    {
        return new WishlistTable();
    }

    protected function loadCurrentState(string $userKey): array
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return array_map(
            static fn(array $wishlistItem) => (int)$wishlistItem['UF_PRODUCT'],
            static::orm()::getList([
                'filter' => ['UF_USER' => $userKey],
                'select' => ['UF_PRODUCT'],
            ])->fetchAll()
        );
    }

    public static function getCurrent(): self
    {
        if (!isset(static::$instances[static::class])) {
            static::$instances[static::class] = new static();
        }

        return static::$instances[static::class];
    }

    public function push(int $productId): Result
    {
        $result = new Result();

        if ($productId <= 0 || in_array($productId, $this->state, true)) {
            return $result->addError(new Error('Error product id or product already exists in state'));
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = static::orm()::add([
            'UF_USER' => User::getUserKey(),
            'UF_PRODUCT' => $productId,
        ]);

        if ($result->isSuccess()) {
            $this->state[] = $productId;

            ItemsAdditionalController::clearCache();
        }

        return $result;
    }

    public function isExists(int $productId): bool
    {
        return in_array($productId, $this->state, true);
    }

    public function remove(int $productId): Result
    {
        $result = new Result();

        if ($productId <= 0 || !in_array($productId, $this->state, true)) {
            return $result->addError(new Error('Error product id or product already exists in state'));
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        $primary = (int)static::orm()::getRow([
            'filter' => [
                'UF_USER' => User::getUserKey(),
                'UF_PRODUCT' => $productId,
            ],
            'select' => ['ID'],
        ])['ID'];

        if ($primary > 0) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $result = static::orm()::delete($primary);

            if ($result->isSuccess()) {
                unset($this->state[array_search($productId, $this->state, true)]);

                ItemsAdditionalController::clearCache();
            }

            return $result;
        }

        return $result;
    }

    public function getState(): array
    {
        return $this->state;
    }

    public function clear(): Result
    {
        $result = new Result();

        /** @noinspection PhpUnhandledExceptionInspection */
        $primaries = Arr::pluck(static::orm()::getList([
            'filter' => ['UF_USER' => User::getUserKey()],
            'select' => ['ID'],
        ])->fetchAll(), 'UF_PRODUCT', 'ID');

        if (!empty($primaries)) {
            foreach ($primaries as $primary => $productId) {
                /** @noinspection PhpUnhandledExceptionInspection */
                $result = static::orm()::delete($primary);

                if (!$result->isSuccess()) {
                    return $result;
                }

                unset($this->state[array_search($productId, $this->state, true)]);
            }
        }

        return $result;
    }

    public function getCount(): int
    {
        return count($this->state);
    }

    public function isEmpty(): bool
    {
        return empty($this->state);
    }

    protected function checkState(array $state): array
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return array_map(
            static fn(array $item) => (int)$item['ID'],
            ElementTable::getList([
                'filter' => ['ID' => $state, 'IBLOCK_ID' => CATALOG_ID],
                'select' => ['ID'],
            ])->fetchAll()
        );
    }
}