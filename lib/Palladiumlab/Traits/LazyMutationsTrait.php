<?php

namespace Palladiumlab\Traits;

use Palladiumlab\Support\Util\Str;
use RuntimeException;

trait LazyMutationsTrait
{
    protected static function callLazyMethod(string $name, array $arguments, $context = null)
    {
        $mutations = ['get', 'set', 'is'];

        foreach ($mutations as $mutation) {
            if ($methodName = static::checkMutation($mutation, $name)) {
                return $context ?
                    $context->{$methodName}(...$arguments)
                    : static::{$methodName}(...$arguments);
            }
        }

        throw new RuntimeException("Method $name not found in " . __CLASS__);
    }

    public static function __callStatic(string $name, array $arguments)
    {
        return static::callLazyMethod($name, $arguments);
    }

    public function __call(string $name, array $arguments)
    {
        return static::callLazyMethod($name, $arguments, $this);
    }

    protected static function checkMutation(string $type, string $name): ?string
    {
        $getterName = $type . Str::ucfirst($name);

        if (method_exists(static::class, $getterName)) {
            return $getterName;
        }

        return null;
    }
}