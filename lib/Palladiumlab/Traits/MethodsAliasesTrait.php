<?php


namespace Palladiumlab\Traits;


use BadMethodCallException;

trait MethodsAliasesTrait
{
    public static function __callStatic($name, $arguments)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        /** @var array $aliasesMap */
        $aliasesMap = static::$aliases ?? [];

        foreach ($aliasesMap as $method => $aliases) {
            if (
                (in_array($name, $aliasesMap, true) || $name === $aliases)
                && !method_exists(static::class, $name)
                && method_exists(static::class, $method)
            ) {
                return static::$method(...$arguments);
            }
        }

        throw new BadMethodCallException("Method $name not found in " . __CLASS__);
    }

    public function __call($name, $arguments)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        /** @var array $aliasesMap */
        $aliasesMap = static::$aliases ?? [];

        foreach ($aliasesMap as $method => $aliases) {
            if (
                (in_array($name, $aliases, true) || $name === $aliases)
                && !method_exists($this, $name)
                && method_exists($this, $method)
            ) {
                return $this->$method(...$arguments);
            }
        }

        throw new BadMethodCallException("Method $name not found in " . __CLASS__);
    }
}