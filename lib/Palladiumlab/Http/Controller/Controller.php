<?php

namespace Palladiumlab\Http\Controller;

use Palladiumlab\Support\Util\Str;
use Palladiumlab\Traits\LazyMutationsTrait;
use Bitrix\Main\Engine\Controller as BitrixController;
use Bitrix\Main\Result;
use ReflectionException;
use WebArch\BitrixCache\Cache;

/**
 * @method static string|null cacheKey()
 * @method static string|null cachePath()
 */
abstract class Controller extends BitrixController
{
    use LazyMutationsTrait;

    public const CACHE_TIME = 60;

    protected function processErrors(Result $result): Result
    {
        if (!$result->isSuccess()) {
            foreach ($result->getErrors() as $error) {
                $this->addError($error);
            }
        }

        return $result;
    }

    /**
     * @param callable $function
     * @return mixed
     * @throws ReflectionException
     */
    protected static function withCache(callable $function)
    {
        return static::cache()
            ->setPath(static::getCachePath())
            ->setKey(static::getCacheKey())
            ->setTTL(static::CACHE_TIME)
            ->callback($function);
    }

    protected static function cache(): Cache
    {
        return Cache::create();
    }

    public static function getCacheKey(): ?string
    {
        return md5(class_basename(static::class));
    }

    public static function getCachePath(): ?string
    {
        return '/api/' . Str::kebab(class_basename(static::class));
    }

    public static function clearCache(): bool
    {
        return static::cache()
            ->setPath(static::getCachePath())
            ->delete(static::getCacheKey());
    }
}