<?php

/** @noinspection PhpUnused */

namespace Palladiumlab\Http\Controller\Api;

use Palladiumlab\Bitrix\Basket\BasketManager;
use Palladiumlab\Http\Controller\Controller;
use Palladiumlab\Support\Bitrix\Bitrix;
use Bitrix\Main\Engine\ActionFilter;
use Exception;

class BasketController extends Controller
{
    protected static ?BasketManager $basket = null;

    protected static function basket(): BasketManager
    {
        Bitrix::modules(['catalog', 'sale']);

        if (!static::$basket) {
            static::$basket = BasketManager::createFromCurrent();
        }

        return static::$basket;
    }

    public function changeOfferAction(int $fromId, int $toId, float $quantity, int $baseProductId = 0): array
    {
        $manager = static::basket();

        if ($oldOffer = $manager->findProduct($fromId)) {
            try {
                $oldOffer->delete();

                $manager->current()->save();
            } catch (Exception $e) {

            }
        }

        return $this->addAction($toId, $quantity, $baseProductId);
    }

    public function addAction(int $id, float $quantity, int $baseProductId = 0, float $price = 0): array
    {
        $props = [];

        if ($baseProductId > 0) {
            $props[] = [
                'NAME' => 'BASE_PRODUCT_ID',
                'CODE' => 'BASE_PRODUCT_ID',
                'VALUE' => $baseProductId,
            ];
        }

        return $this->processErrors(
            static::basket()
                ->addProduct($id, $quantity, $props, $price)
        )->getData();
    }

    public function setQuantityAction(int $id, float $quantity): array
    {
        return $this->processErrors(
            static::basket()
                ->setProductQuantity($id, $quantity)
        )->getData();
    }

    public function deleteAction(int $id): array
    {
        return $this->processErrors(
            static::basket()
                ->removeProduct($id)
        )->getData();
    }

    public function clearAction(): array
    {
        return $this->processErrors(
            static::basket()
                ->clear()
        )->getData();
    }

    public function clearUnavailableAction(): array
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->processErrors(
            static::basket()
                ->clearUnavailable()
        )->getData();
    }

    public function configureActions(): array
    {
        return [
            'changeOffer' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Csrf(),
                ],
            ],
            'add' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Csrf(),
                ],
            ],
            'setQuantity' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Csrf(),
                ],
            ],
            'delete' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Csrf(),
                ],
            ],
            'clear' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Csrf(),
                ],
            ],
            'clearUnavailable' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Csrf(),
                ],
            ],
        ];
    }
}