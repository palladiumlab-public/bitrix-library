<?php

namespace Palladiumlab\Http\Controller\Api;

use Palladiumlab\Catalog\Wishlist as WishlistManager;
use Palladiumlab\Http\Controller\Controller;
use Bitrix\Main\Engine\ActionFilter;

class WishlistController extends Controller
{
    public function getAction(): array
    {
        return ['items' => WishlistManager::current()->state()];
    }

    public function addAction(int $id): array
    {
        return $this->processErrors(WishlistManager::current()->push($id))->getData();
    }

    public function deleteAction(int $id): array
    {
        return $this->processErrors(WishlistManager::current()->remove($id))->getData();
    }

    public function configureActions(): array
    {
        return [
            'get' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_GET]
                    ),
                    new ActionFilter\Csrf(),
                ],
            ],
            'add' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Csrf(),
                ],
            ],
            'delete' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                    new ActionFilter\HttpMethod(
                        [ActionFilter\HttpMethod::METHOD_POST]
                    ),
                    new ActionFilter\Csrf(),
                ],
            ],
        ];
    }
}