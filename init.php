<?php

use Bitrix\Sale\Registry;
use Dotenv\Dotenv;
use Palladiumlab\Bitrix\Sale;
use Palladiumlab\Support\Bitrix\Bitrix;

if (!defined('ROOT_PATH')) {
    /** Document root */
    define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT']);
}

if (!defined('LOG_PATH')) {
    define('LOG_PATH', ROOT_PATH . '/logs');
}

if (Bitrix::modules('sale')) {
    try {
        Registry::getInstance(Sale\Order::getRegistryType())->set(Sale\Order::getRegistryEntity(), Sale\Order::class);
        Registry::getInstance(Sale\PropertyValue::getRegistryType())->set(Sale\PropertyValue::getRegistryEntity(), Sale\PropertyValue::class);
        Registry::getInstance(Sale\BasketItem::getRegistryType())->set(Sale\BasketItem::getRegistryEntity(), Sale\BasketItem::class);
    } catch (Exception $e) {

    }
}

if (file_exists(__DIR__ . '/functions.php')) {
    /** Подключение файла глобальных методов (функций) */
    require_once __DIR__ . '/functions.php';
}

if (file_exists(__DIR__ . '/events.php')) {
    /** Подключение файла событий */
    require_once __DIR__ . '/events.php';
}

/** Загрузка файла /.env с переменными окружения */
Dotenv::createImmutable($_SERVER['DOCUMENT_ROOT'])->safeLoad();
